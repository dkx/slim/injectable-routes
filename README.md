# DKX/InjectableRoutes

Inject custom arguments into slim route callbacks

**Supports only class routes!**

## Installation

```bash
$ composer require dkx/slim-injectable-routes
```

## Usage

```php
<?php

use DKX\SlimInjectableRoutes\InjectableRoutes;
use DKX\MethodInjector\MethodInjector;
use Slim\Container;

$c = new Container();
$c['foundHandler'] = function() {
    $routes = new InjectableRoutes;
    $routes->provideInjectableSetup(function(MethodInjector $injector) {
        $injector->provideValue(\stdClass::class, new \stdClass);
    });
    
    return $routes;
};
```

Now you will be able to automatically inject the `stdClass` into your route like this:

```php
<?php

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

final class MyRoute
{

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args, \stdClass $std): ResponseInterface
    {
        // ...
        return $response;    
    }

}
```

To find more information about the `MethodInjector` see the documentation for 
[dkx/method-injector](https://gitlab.com/dkx/php/method-injector).
