<?php

declare(strict_types=1);

namespace DKX\SlimInjectableRoutes;

use DKX\MethodInjector\MethodInjector;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\InvocationStrategyInterface;

final class InjectableRoutes implements InvocationStrategyInterface
{


	/** @var callable[] */
	private $setup = [];


	public function provideInjectableSetup(callable $setup): void
	{
		$this->setup[] = $setup;
	}


	public function __invoke(callable $callable, ServerRequestInterface $request, ResponseInterface $response, array $routeArguments)
	{
		foreach ($routeArguments as $k => $v) {
			$request = $request->withAttribute($k, $v);
		}

		$injector = new MethodInjector;

		foreach ($this->setup as $setup) {
			call_user_func($setup, $injector, $request, $response, $routeArguments, $callable);
		}

		return $injector->callMethod($callable, [$request, $response, $routeArguments]);
	}

}
