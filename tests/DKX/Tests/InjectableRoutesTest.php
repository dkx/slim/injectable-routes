<?php

declare(strict_types=1);

namespace DKX\Tests;

use DKX\SlimInjectableRoutes\InjectableRoutes;
use DKX\MethodInjector\MethodInjector;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class InjectableRoutesTest extends TestCase
{


	/** @var \DKX\Tests\TestingRoute */
	private $route;

	/** @var \DKX\SlimInjectableRoutes\InjectableRoutes */
	private $routes;

	/** @var \Psr\Http\Message\ServerRequestInterface */
	private $request;

	/** @var \Psr\Http\Message\ResponseInterface */
	private $response;


	public function setUp(): void
	{
		parent::setUp();

		$this->route = new TestingRoute;
		$this->routes = new InjectableRoutes;
		$this->request = \Mockery::mock(ServerRequestInterface::class);
		$this->response = \Mockery::mock(ResponseInterface::class);
	}


	public function tearDown(): void
	{
		parent::tearDown();

		\Mockery::close();
	}


	public function testInvoke_simple(): void
	{
		$called = $this->routes->__invoke([$this->route, 'helloWorld'], $this->request, $this->response, []);

		self::assertSame('hello world', $called);
	}


	public function testInvoke_inject(): void
	{
		$std = new \stdClass;
		$this->routes->provideInjectableSetup(function (MethodInjector $injector, ServerRequestInterface $request, ResponseInterface $response, array $routeArguments, callable $callable) use ($std) {
			$injector->provideValue(\stdClass::class, $std);
		});

		$called = $this->routes->__invoke([$this->route, 'stdClass'], $this->request, $this->response, []);

		self::assertSame($std, $called);
	}

}


class TestingRoute
{


	public function helloWorld(ServerRequestInterface $request, ResponseInterface $response): string
	{
		return 'hello world';
	}


	public function stdClass(ServerRequestInterface $request, ResponseInterface $response, array $args, \stdClass $std): \stdClass
	{
		return $std;
	}

}
